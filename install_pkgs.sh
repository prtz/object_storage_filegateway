#!/bin/sh
sudo apt update
sudo apt upgrade

# .local zone resolve desision
sudo ln -sf /run/systemd/resolve/resolv.conf  /etc/resolv.conf

sudo -i

# S3 credentials config
sudo mkdir ~/.aws

read -p 'Access key ID: ' keyid
read -p 'Secret access key: ' secretkey
cat > ~/.aws/credentials << EOF
[default]
aws_access_key_id = $keyid
aws_secret_access_key = $secretkey
EOF

cat > ~/.aws/config << EOF
[default]
region = ru-central1
output = yaml
EOF

# geesefs install and config
sudo wget https://github.com/yandex-cloud/geesefs/releases/latest/download/geesefs-linux-amd64
sudo cp ~/geesefs-linux-amd64 /usr/local/sbin/geesefs

read -p 'Bucket name: ' bucketname
sudo mkdir /$bucketname
cat > /etc/fstab << EOF
$bucketname    /$bucketname    fuse.geesefs    _netdev,allow_other,--file-mode=0666,--dir-mode=0777,--storage-class=STANDARD_IA    0    0
EOF


# Packages install
PACKAGES="nfs-kernel-server samba samba-common samba-dsdb-modules samba-vfs-modules winbind libnss-winbind libpam-winbind krb5-config"
sudo apt-get -y --force-yes install $PACKAGES

# NFS config
read -p 'IP or CIDR where NFS need: ' nfsip
cat > /etc/exports << EOF
/data $nfsip(rw,no_subtree_check,fsid=100)
/data 127.0.0.1(rw,no_subtree_check,fsid=100)
EOF

#services enable and start
sudo systemctl enable smbd
sudo systemctl start smbd
sudo systemctl enable nmbd
sudo systemctl start nmbd
sudo systemctl enable winbind
sudo systemctl start winbind